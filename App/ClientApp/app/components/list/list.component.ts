﻿import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';

@Component({
    selector: 'list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']

})
export class ListComponent {
    public users: User[] = [];
    public user: User;

    constructor(http: Http, @Inject('BASE_URL') baseUrl: string) {
        http.get(baseUrl + 'api/users').subscribe(result => {
            let response: User[] = result.json() as User[];
            for (let u of response) {
                if (u.id === 1) {
                    this.user = u;
                }
                else {
                    this.users.push(u);
                }
            }
        }, error => console.error(error));
    }
}

interface User {
    id: number;
    name: string;
    photoUrl: string;
}
