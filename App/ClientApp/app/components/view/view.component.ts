import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Http } from '@angular/http';

@Component({
    selector: 'view',
    templateUrl: './view.component.html',
    styleUrls: ['./view.component.css']

})
export class ViewComponent implements OnInit {
    public items: FeedbackItem[] = [];
    public users: User[] = [];
    public user: User;
    public id: number;
    public baseUrl: string;
    constructor(private http: Http, @Inject('BASE_URL') url: string, private route: ActivatedRoute, router: Router) {
        this.baseUrl = url;
        this.user = new User();
        this.user.photoUrl = "avatar.jpg";
        http.get(this.baseUrl + 'api/items').subscribe(result => {
            this.items = result.json() as FeedbackItem[];
            this.items = this.items.sort((a, b) => { return a.id < b.id ? -1 : a.id > b.id ? 1 : 0 });
        }, error => console.error(error));
        http.get(this.baseUrl + 'api/users').subscribe(result => {
            this.users = result.json() as User[];
        }, error => console.error(error));
    }

    ngOnInit(): void {

        this.route.params.forEach((params: Params) => {
            let id = +params['id']; // (+) converts string 'id' to a number
            this.http.get(this.baseUrl + 'api/users/' + id).subscribe(result => {
                this.user = result.json() as User;
                let instances: FeedbackItemInstance[] = [];
                let average = 0;
                console.log("1");

                for (var f of this.user.feedbacks) {
                    f.user = this.users.find(e => { return e.id == f.feedbackFromUser });
                    instances = instances.concat(f.scores);
                    average += f.score;
                }
                console.log("Ưnstances" + JSON.stringify(instances));


                average = this.user.feedbacks.length > 0 ? average * 1.0 / this.user.feedbacks.length : 0;
                this.user.score = Math.round(average * 10) / 10;

                for (let i of this.items) {
                    let itemScores = instances.filter(e => { return e.feedbackItemId === i.id });
                    console.log(i.id + JSON.stringify(itemScores));

                    i.score = 0;
                    for (let is of itemScores) {
                        i.score += is.score;
                    }
                    i.score = itemScores.length > 0 ? i.score / itemScores.length : 0;
                    i.score = Math.round(i.score * 10) / 10;
                }

                console.log(JSON.stringify(this.items));

            }, error => console.error(error));
        });
    }
}

export class User {
    id: number;
    name: string;
    photoUrl: string;
    feedbacks: Feedback[];
    score: number;
}

export class FeedbackItem {
    id: number;
    name: string;
    score: number;
}


export class FeedbackItemInstance {
    feedbackItemId: number;
    score: number;
}

export class Feedback {
    id: number;
    message: string;
    feedbackFromUser: number;
    feedbackToUser: number;
    scores: FeedbackItemInstance[];
    score: number;
    photoUrls: string[];
    user: any;
}
