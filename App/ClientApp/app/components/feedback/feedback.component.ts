import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Http } from '@angular/http';
import * as $ from 'jquery';

@Component({
    selector: 'feedback',
    templateUrl: './feedback.component.html',
    styleUrls: ['./feedback.component.css']

})
export class FeedbackComponent implements OnInit {
    public items: FeedbackItem[] = [];
    public user = {id: 0, photoUrl: 'avatar.jpg'};
    public id: number;
    public baseUrl: string;
    public sum: number = 0;
    
    public feedback: Feedback;
    constructor(private http: Http, @Inject('BASE_URL') url: string, private route: ActivatedRoute, private router: Router) {
        this.baseUrl = url;
        this.feedback = new Feedback();
        this.feedback.feedbackFromUser = 1;
        this.feedback.photoUrls = ["", "", ""];
        this.feedback.score = 0;
        http.get(this.baseUrl + 'api/items').subscribe(result => {
            this.items = result.json() as FeedbackItem[];
            this.items = this.items.sort((a, b) => { return a.id < b.id ? -1 : a.id > b.id ? 1 : 0 });

            this.feedback.scores = [];
            this.feedback.message = "";
            for (var item of this.items) {
                let s = new FeedbackItemInstance();
                s.feedbackItemId = item.id;
                s.score = item.score ? item.score : 0;
                this.feedback.scores.push(s);
            }
        }, error => console.error(error));
    }

    ngOnInit(): void {

        this.route.params.forEach((params: Params) => {
            this.id = +params['id']; // (+) converts string 'id' to a number
            this.http.get(this.baseUrl + 'api/users/' + this.id).subscribe(result => {
                this.user = result.json() as User;
                this.feedback.feedbackToUser = this.user.id;

            }, error => console.error(error));
           
        });
    }

    calculateScore() {
        console.log("clicked");
        this.sum = 0;
        this.feedback.scores = [];
        for (var item of this.items) {
            console.log(item.score);
            console.log(this.sum);
            this.sum += item.score ? item.score : 0;
            let s = new FeedbackItemInstance();
            s.feedbackItemId = item.id;
            s.score = item.score ? item.score : 0;
            this.feedback.scores.push(s);
        }
        this.sum = this.sum * 1.0 / this.items.length;
        console.log("Sum: " + this.sum);
        this.feedback.score = this.sum;

    }

    upload(i: number) {
        let current = this;
        $("#file" + i).trigger("click");
        $("#file" + i).off("change");
        $("#file" + i).on('change', null, (e) => {

            console.log("change");
            var input = <HTMLInputElement>e.target;
            var files = input.files; //give you intellisense on files
            if (files && files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    console.log("reader.onload");
                    console.log(e);
                    $('#img' + i).attr('src', (e.target as FileReader).result);
                    $('#img' + i).show();
                    $('.upload[data-id="' + i + '"]').hide();
                    current.feedback.photoUrls[i - 1] = (e.target as FileReader).result;
                }
                reader.readAsDataURL(input.files![0]);
            }
        });
    }

    save() {
        console.log(JSON.stringify(this.feedback));
        this.http.post(this.baseUrl + 'api/feedback', this.feedback).subscribe(result => {
            console.log(result);
            this.router.navigateByUrl("view/" + this.id);
        }, error => console.error(error));
    }
}

interface User {
    id: number;
    name: string;
    photoUrl: string;
}

interface FeedbackItem {
    id: number;
    name: string;
    score: number;
}

export class FeedbackItemInstance {
    feedbackItemId: number;
    score: number;
}

export class Feedback {
    id: number;
    message: string;
    feedbackFromUser: number;
    feedbackToUser: number;
    scores: FeedbackItemInstance[];
    score: number;
    photoUrls: string[];
}
