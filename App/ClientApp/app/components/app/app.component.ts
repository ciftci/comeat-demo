import { Component } from '@angular/core';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    public user: User;
    constructor() {
        this.user = new User();
        this.user.id = 1;
        this.user.name = "Mustafa Suleyman Ciftci";
        this.user.photoUrl = "https://comeeat.blob.core.windows.net/demo/suleyman.jpg";
    }
}


export class User {
    id: number;
    name: string;
    photoUrl: string;
}
