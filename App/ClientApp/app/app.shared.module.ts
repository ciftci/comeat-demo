import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { FetchDataComponent } from './components/fetchdata/fetchdata.component';
import { CounterComponent } from './components/counter/counter.component';
import { ListComponent } from './components/list/list.component';
import { ViewComponent } from './components/view/view.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { BarRatingModule } from "ngx-bar-rating";


@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        CounterComponent,
        FetchDataComponent,
        ListComponent,
        HomeComponent,
        ViewComponent,
        FeedbackComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        BarRatingModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'list', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'counter', component: CounterComponent },
            { path: 'fetch-data', component: FetchDataComponent },
            { path: 'list', component: ListComponent },
            { path: 'view/:id', component: ViewComponent },
            { path: 'feedback/:id', component: FeedbackComponent },
            { path: '**', redirectTo: 'list' }
        ])
    ]
})
export class AppModuleShared {
}
