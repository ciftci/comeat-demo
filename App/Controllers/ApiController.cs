﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using App.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;

namespace App.Controllers
{
    [Produces("application/json")]
    [Route("api")]
    public class ApiController : Controller
    {
        private readonly IRepository _repository;
        private readonly Settings _settings;

        public ApiController(IRepository repository, IOptions<Settings> settings)
        {
            _repository = repository;
            _settings = settings.Value;
        }
        



        [HttpGet("admin/{setting}")]
        public string Init(string setting)
        {
            if (setting == "init")
            {
                _repository.RemoveAllFeedbacks();

                _repository.RemoveAllUsers();
                _repository.AddUser(new User { Id = 1, Name = "Mustafa Süleyman Çiftçi", PhotoUrl = _settings.StorageUrl + _settings.StorageFilePath + "suleyman.jpg" });
                _repository.AddUser(new User { Id = 2, Name = "Kaan Özdokmeci", PhotoUrl = _settings.StorageUrl + _settings.StorageFilePath + "kaan.jpg" });
                _repository.AddUser(new User { Id = 3, Name = "Sofie", PhotoUrl = _settings.StorageUrl + _settings.StorageFilePath + "avatar.jpg" });
                _repository.AddUser(new User { Id = 4, Name = "Yuvraj", PhotoUrl = _settings.StorageUrl + _settings.StorageFilePath + "avatar.jpg" });
                _repository.AddUser(new User { Id = 5, Name = "Cem Çiftgül", PhotoUrl = _settings.StorageUrl + _settings.StorageFilePath + "avatar.jpg" });
                _repository.RemoveAllFeedbackItems();
                _repository.AddFeedbackItem(new FeedbackItem { Id = 1, Name = "Online Communication" });
                _repository.AddFeedbackItem(new FeedbackItem { Id = 2, Name = "Sociability" });
                _repository.AddFeedbackItem(new FeedbackItem { Id = 3, Name = "Athmosphere" });
                _repository.AddFeedbackItem(new FeedbackItem { Id = 4, Name = "Food" });
                _repository.AddFeedbackItem(new FeedbackItem { Id = 5, Name = "Value for money" });
                return "Done";
            }

            return "Unknown";
        }

        [HttpGet("users")]
        public Task<IEnumerable<User>> GetUsers()
        {
            return GetUsersInternal();
        }
        private async Task<IEnumerable<User>> GetUsersInternal()
        {
            return await _repository.GetAllUsers();
        }

        [HttpGet("users/{id}")]
        public Task<User> GetUser(int id)
        {
            return GetUserDetailsInternal(id);
        }
        private async Task<User> GetUserDetailsInternal(int id)
        {
            return await _repository.GetUserDetails(id);
        }

        [HttpGet("items")]
        public Task<IEnumerable<FeedbackItem>> GetFeedbackItems()
        {
            return GetFeedbackItemsInternal();
        }
        private async Task<IEnumerable<FeedbackItem>> GetFeedbackItemsInternal()
        {
            return await _repository.GetAllFeedbackItems();
        }

        [HttpPost("feedback")]
        public Task AddFeedback([FromBody]Feedback feedback)
        {
            return AddFeedbackInternal(feedback);
        }
        private async Task AddFeedbackInternal(Feedback feedback)
        {
            var storageAccount = CloudStorageAccount.Parse(_settings.StorageConnectionString);
            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference(_settings.StorageFilePath);
            var urls = new List<string>();
            foreach(var url in feedback.PhotoUrls)
            {
                if (!string.IsNullOrEmpty(url))
                {
                    var fileName = Guid.NewGuid().ToString().ToLower().Replace("-", "");
                    var blockBlob = container.GetBlockBlobReference(fileName);
                    var data = Convert.FromBase64String(url.Split(",")[1]);
                    blockBlob.UploadFromByteArrayAsync(data,0,data.Length).GetAwaiter().GetResult();
                    urls.Add(fileName);
                }
            }
            feedback.PhotoUrls = urls;

            await _repository.AddFeedback(feedback);
        }
    }
}