﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Models
{
    public class Settings
    {
        public string ConnectionString;
        public string Database;
        public string StorageUrl;
        public string StorageConnectionString;
        public string StorageFilePath;
    }
}
