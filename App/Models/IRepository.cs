﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Models
{
    public interface IRepository
    {
        Task<IEnumerable<Feedback>> GetAllFeedbacks();
        Task<Feedback> GetFeedback(string id);
        
        Task AddFeedback(Feedback item);
        
        
        Task<bool> RemoveAllFeedbacks();

        Task<IEnumerable<User>> GetAllUsers();
        Task AddUser(User item);
        Task<bool> RemoveAllUsers();


        Task<IEnumerable<FeedbackItem>> GetAllFeedbackItems();
        Task AddFeedbackItem(FeedbackItem item);
        Task<bool> RemoveAllFeedbackItems();

        Task<User> GetUserDetails(int id);
        

    }
}
