﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Models
{
    public class Repository : IRepository
    {
        private readonly Context _context = null;

        public Repository(IOptions<Settings> settings)
        {
            _context = new Context(settings);
        }

        public async Task<IEnumerable<Feedback>> GetAllFeedbacks()
        {
            try
            {
                return await _context.Feedbacks
                        .Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<Feedback> GetFeedback(string id)
        {
            var filter = Builders<Feedback>.Filter.Eq("Id", id);

            try
            {
                return await _context.Feedbacks
                                .Find(filter)
                                .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task AddFeedback(Feedback item)
        {
            try
            {
                item.Id = Guid.NewGuid().ToString();     
                await _context.Feedbacks.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<bool> RemoveFeedback(string id)
        {
            try
            {
                DeleteResult actionResult
                    = await _context.Feedbacks.DeleteOneAsync(
                        Builders<Feedback>.Filter.Eq("Id", id));

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }


        public async Task<bool> RemoveAllFeedbacks()
        {
            try
            {
                DeleteResult actionResult
                    = await _context.Feedbacks.DeleteManyAsync(new BsonDocument());

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public Task<bool> UpdateFeedbackDocument(string id, string body)
        {
            throw new NotImplementedException();
        }


        public async Task<IEnumerable<User>> GetAllUsers()
        {
            try
            {
                return await _context.Users
                        .Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task AddUser(User item)
        {
            try
            {
                await _context.Users.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
        public async Task<bool> RemoveAllUsers()
        {
            try
            {
                DeleteResult actionResult
                    = await _context.Users.DeleteManyAsync(new BsonDocument());

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<IEnumerable<FeedbackItem>> GetAllFeedbackItems()
        {
            try
            {
                return await _context.FeedbackItems
                        .Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task AddFeedbackItem(FeedbackItem item)
        {
            try
            {
                await _context.FeedbackItems.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
        public async Task<bool> RemoveAllFeedbackItems()
        {
            try
            {
                DeleteResult actionResult
                    = await _context.FeedbackItems.DeleteManyAsync(new BsonDocument());

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<User> GetUserDetails(int id)
        {
            try
            {
                var user = await _context.Users
                        .Find(e=>e.Id == id).FirstOrDefaultAsync();
                var feedBacks = await _context.Feedbacks.Find(e => e.FeedbackFromUser == id || e.FeedbackToUser == id).ToListAsync();
                user.Feedbacks = feedBacks;
                return user;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
    }
}
