﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Models
{
    public class FeedbackItemInstance
    {
        public int FeedbackItemId { get; set; }
        public double Score { get; set; }
    }
}
