﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Models
{
    public class Feedback
    {
        [BsonId]
        public string Id { get; set; }
        public string Message { get; set; } = string.Empty;
        public short Type { get; set; }
        public int FeedbackFromUser { get; set; }
        public int FeedbackToUser { get; set; }
        public List<FeedbackItemInstance> Scores { get; set; }
        public double Score { get; set; }
        public List<string> PhotoUrls { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
    }
}
