﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Models
{
    public class Context
    {
        private readonly IMongoDatabase _database = null;

        public Context(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }

        public IMongoCollection<Feedback> Feedbacks
        {
            get
            {
                return _database.GetCollection<Feedback>("Feedback");
            }
        }
        public IMongoCollection<User> Users
        {
            get
            {
                return _database.GetCollection<User>("User");
            }
        }
        public IMongoCollection<FeedbackItem> FeedbackItems
        {
            get
            {
                return _database.GetCollection<FeedbackItem>("FeedbackItem");
            }
        }
    }
}
